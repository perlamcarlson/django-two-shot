from django.db import models
from django.views import View
from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your models here.
class ReceiptListView(LoginRequiredMixin, TemplateView):
    template_name = "list.html"
    login_url = "/login/"
    redirect_field_name = "redirect_to"
